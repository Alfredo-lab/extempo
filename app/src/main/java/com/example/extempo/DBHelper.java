package com.example.extempo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 20    ;
    private static final String NAME_DB = "database.db";

    private static String SQL_CALIF = "DROP TABLE IF EXISTS " + Tables.CalificacionesTabla.TableName;

    public DBHelper(Context context) {
        super(context, NAME_DB, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.tablaCalificaciones());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_CALIF);
        onCreate(db);
    }

    public String tablaCalificaciones(){
        SQLiteTable table = new SQLiteTable(Tables.CalificacionesTabla.TableName, Tables.CalificacionesTabla.ID);
        table.addColumn(Tables.CalificacionesTabla.calificacion, table.TYPE_REAL);
        table.addColumn(Tables.CalificacionesTabla.matricula, table.TYPE_TEXT);
        table.addColumn(Tables.CalificacionesTabla.grupo, table.TYPE_TEXT);
        table.addColumn(Tables.CalificacionesTabla.materia, table.TYPE_TEXT);
        return table.getQuery();
    }
}
