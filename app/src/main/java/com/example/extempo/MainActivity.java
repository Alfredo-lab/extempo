package com.example.extempo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((Button) findViewById(R.id.btnSalir)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((Button) findViewById(R.id.btnRegistrar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grupo = ((Spinner) findViewById(R.id.spGrupos)).getSelectedItem().toString();
                Intent intent = new Intent(MainActivity.this, RegistroCalificaciones.class);
                intent.putExtra("grupo", grupo);
                startActivity(intent);
            }
        });

    }
}
