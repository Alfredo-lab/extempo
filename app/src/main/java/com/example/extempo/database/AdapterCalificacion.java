package com.example.extempo.database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.extempo.R;

import java.util.ArrayList;

public class AdapterCalificacion extends ArrayAdapter<Calficacion> {

    public AdapterCalificacion(Context context, ArrayList<Calficacion> calificaciones) {
        super(context, 0, calificaciones);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Calficacion index = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_calificaciones, parent, false);
        }
        TextView lb1 = (TextView) convertView.findViewById(R.id.lbCalificacion);
        TextView lb2 = (TextView) convertView.findViewById(R.id.lbMateria);
        TextView lb3 = (TextView) convertView.findViewById(R.id.lbMatricula);
        lb1.setText(String.valueOf(index.getCalficiacion()));
        lb2.setText(index.getMateria());
        lb3.setText(index.getMatircula());
        // Return the completed view to render on screen
        return convertView;
    }
}
