package com.example.extempo.database;

public class Tables {

    public static class CalificacionesTabla{
        public static final String TableName = "calificaciones";
        public static final String ID = "id";
        public static final String matricula = "matricula";
        public static final String grupo = "grupo";
        public static final String calificacion = "calificaciones";
        public static final String materia = "materia";
    }
}
