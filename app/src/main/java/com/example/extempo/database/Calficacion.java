package com.example.extempo.database;

public class Calficacion {
    private String materia;
    private String matircula;
    private float calficiacion;
    private String grupo;

    public Calficacion(String materia, String matircula, float calficiacion) {
        this.materia = materia;
        this.matircula = matircula;
        this.calficiacion = calficiacion;
    }

    public Calficacion()
    {

    }


    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getMatircula() {
        return matircula;
    }

    public void setMatircula(String matircula) {
        this.matircula = matircula;
    }

    public float getCalficiacion() {
        return calficiacion;
    }

    public void setCalficiacion(float calficiacion) {
        this.calficiacion = calficiacion;
    }
}
